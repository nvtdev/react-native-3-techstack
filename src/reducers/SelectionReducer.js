export default (state = null, action) => {
  // the '= null' above is to avoid returning undefined when the state is not defined
  switch (action.type) {
      case 'select_library':
        return action.payload;
      default:
        return state;
  }
};
