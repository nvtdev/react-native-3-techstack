export const selectLibrary = (libraryId) => {
  // an action is always an object with type property
  return {
      type: 'select_library',
      payload: libraryId
  };
};
